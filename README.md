# cells-amount-list

Component to show a list of amounts with label and quantity. It has the option
to add primary, seconday and tertiary labels. Also can show a highlight of the label and quantity.

Example:
```html
<cells-amount-list></cells-amount-list>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-amount-list-scope      | scope description | default value  |
| --cells-amount-list-list_item  | item of the list     | {}             |
| --cells-amount-list-item_label  | label of the item     | {}             |
| --cells-amount-list-item-final  | final label and amount wrapper     | {}             |
| --cells-amount-list-final_amount  | final amount     | {}             |
| --cells-amount-list-item_label-final  | final label    | {}             |
| --cells-amount-list-final  | final wrapper     | {}             |
| --cells-amount-list-triangle-primary_color_helper  | primary triangle color    |--bbva-core-blue, #004481             |
| --cells-amount-list-triangle-secondary_color_helper  | secondary triangle color     |--bbva-medium-blue, #2A86CA             |
| --cells-amount-list-triangle-tertiary_color_helper  | tertiary triangle color     |--bbva-light-yellow, #FADE8E             |
| --cells-amount-list-highlight_helper  | highlight a section     | uppercase, bold          |
| --cells-amount-list-border_helper  | add a border top     |  {} | 


(function() {

  'use strict';

  Polymer({

    is: 'cells-amount-list',

    behaviors: [
      Polymer.i18nBehavior
    ],
    properties: {
      /** Items that are gonna show in the list
      * @type {Array}
      * @public
      */
      items: {
        type: Array,
        value: function() {
          return [];
        }
      },
      /** Final result
      * @type {Object}
      * @public
      */
      final: {
        type: Object
      }
    },
    /**
    * Checks if has 'final' value to show it in the list
    */
    _checkedValue: function(str) {
      return Boolean(str);
    }

  });

}());

var templateBind = document.getElementById('tbind');

// The dom-change event signifies when the template has stamped its DOM.
templateBind.addEventListener('dom-change', function() {
  var myEl2 = document.getElementById('demo2');
  var data = [
    {
      key: 'Saldo anterior',
      value: {
        amount: 10303,
        currency: 'MXN',
        localCurrency: 'MXN'
      }
    },
    {
      key: 'Nuevos cargos',
      value: {
        amount: 10303,
        currency: 'MXN',
        localCurrency: 'MXN'
      }
    },
    {
      key: 'Nuevos pagos',
      value: {
        amount: 10303,
        currency: 'MXN',
        localCurrency: 'MXN'
      }
    },
  ];
  var data2 = [
    {
      key: 'Monto en transito',
      value: {
        amount: 10303,
        currency: 'MXN',
        localCurrency: 'MXN'
      },
      css: 'secondary'
    },
    {
      key: 'Monto disponible',
      value: {
        amount: 10303,
        currency: 'MXN',
        localCurrency: 'MXN'
      },
      css: 'tertiary'
    }
  ];
  var final2 = {
    key: 'Línea de crédito',
    value: {
      amount: 10303,
      currency: 'MXN',
      localCurrency: 'MXN'
    },
    css: 'highlight'
  };
  var final = {
    key: 'Monto utilizado',
    value: {
      amount: 10303,
      currency: 'MXN',
      localCurrency: 'MXN'
    },
    css: 'highlight primary border'
  };
  templateBind.set('isMobile', false);
  templateBind.set('items', data);
  myEl2.set('items', data2);
  myEl2.set('final', final2);
  templateBind.set('final', final);
  templateBind.checkedMobile = function checkedMobile(isMobile) {
    return (isMobile) ? 'mobile' : '';
  };
});

document.addEventListener('WebComponentsReady', function() {
  // set component properties here
});
